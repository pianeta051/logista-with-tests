Rails.application.routes.draw do

  resources :ngos, except: [:index] do

    resources :requests
    resources :projects, except: [:index, :show, :edit, :update, :destroy]
    # Projects
    get 'search', to: 'projects#search', as: :project_search
    get 'projects', to: 'ngos#projects', as: :projects, on: :member


    post 'join', to: 'ngos#join', as: :join, on: :member
    get 'join-ngo', to: 'ngos#select', as: :join, on: :collection
    put 'make_admin', to: 'ngos#make_admin', as: :make_admin
    put 'make_worker', to: 'ngos#make_worker', as: :make_worker
    put 'stoping_admin', to: 'ngos#stoping_admin', as: :stoping_admin
    delete 'destroy_association', to: 'ngos#destroy_association', as: :destroy_association, on: :member

    resources :participants, except: [:new, :create]
    #resources :participants, except: [:new, :create]

    get 'team/:team_id/participants/new', to: 'participants#new', as: :new_participant
    post 'team/:team_id/participants', to: 'participants#create', as: :create_participant
  end

  get 'my-ngos', to: 'ngos#my_ngos', as: :my_ngos

  resources :teams, except: :index do
    member do
      put 'add-participant/:participant_id', to: 'teams#add_participant', as: :add_participant
      put 'remove-participant_team/:participant_id', to: 'teams#remove_participant', as: :remove_participant
      put 'make-leader/:participant_id', to: 'teams#make_leader', as: :make_leader
      put 'make-regular/:participant_id', to: 'teams#make_regular', as: :make_regular
    end
  end

  resources :projects, only: [:index, :show, :edit, :update, :destroy, :add_user, :delete_user] do
    member do
      put 'add_user', to: 'projects#add_user', as: :add_user
      put 'delete_user', to: 'projects#delete_user', as: :delete_user
    end
  end

  get 'search', to: 'projects#search', as: :project_search
  delete 'handle-request', to: 'requests#handle', as: :handle_request
  get 'requests', to: 'requests#index', as: :requests

  # User routes
  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }
  as :user do
    get     'sign-up', to: 'devise/registrations#new', as: :sign_up
    get     'sign-in', to: 'devise/sessions#new', as: :sign_in
    delete 'sign-out', to: 'devise/sessions#destroy', as: :sign_out
    get    'profile',  to: 'devise/registrations#edit', as: :profile
  end

  get 'not-found', to: 'pages#not_found', as: :not_found
  get '/change_locale/:locale', to: 'locales#change_locale', as: :change_locale

  root 'pages#home'


    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

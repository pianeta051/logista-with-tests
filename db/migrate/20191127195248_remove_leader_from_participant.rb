class RemoveLeaderFromParticipant < ActiveRecord::Migration[5.2]
  def change
    remove_column :participants, :leader, :boolean
  end
end

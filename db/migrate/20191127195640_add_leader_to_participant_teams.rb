class AddLeaderToParticipantTeams < ActiveRecord::Migration[5.2]
  def change
    add_column :participant_teams, :leader, :boolean
  end
end

class AddNgoIdToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :ngo_id, :integer
  end
end

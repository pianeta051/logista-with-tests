class RemoveNameFromTeam < ActiveRecord::Migration[5.2]
  def change
    remove_column :teams, :name, :string
  end
end

class RemoveNoAdminFromNgouser < ActiveRecord::Migration[5.2]
  def change
    remove_column :ngo_users, :no_admin, :boolean
  end
end

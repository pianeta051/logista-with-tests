class CreateNgoUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :ngo_users do |t|
      t.references :user, foreign_key: true
      t.references :ngo, foreign_key: true
      t.boolean :admin

      t.timestamps
    end
  end
end

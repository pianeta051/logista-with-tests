class AddCountryCodeToParticipant < ActiveRecord::Migration[5.2]
  def change
    add_column :participants, :country_code, :string
  end
end


puts "Creating default user"
default_user = FactoryBot.create(:user, email: 'usuario@usuario.com', password: 'password')

ngos = FactoryBot.create_list(:ngo, 5)
ngos.each do |ngo|
  puts "NGO: "+ngo.name

  puts "--Creating admin user..."
  FactoryBot.create(:ngo_user, ngo: ngo, admin:true)
  ngo.add_admin(default_user)

  puts "--Creating workers..."
  FactoryBot.create_list(:ngo_user, 2, ngo: ngo, admin: false)

  puts "--Creating projects..."
  projects = FactoryBot.create_list(:project, 2, ngo: ngo)
  projects.each do |project|
    puts "--Project: "+project.name

    puts "----Creating teams..."
    teams = FactoryBot.create_list(:team, 3, project: project)

    teams.each do |team|
      puts "----Team: " + team.country

      puts "------Creating participants..."
      leader = FactoryBot.create(:participant, nationality: team.country)
      leader.join(team, leader: true)
      4.times do
        participant = FactoryBot.create(:participant, nationality: team.country)
        participant.join(team)
      end
    end
  end
end

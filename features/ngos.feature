Feature: ngos
I should be able to publish and see ngos in the app

  Background:
  Given I am a registered user
  And I sign in


  Scenario: Adding a new ngo
    When I visit the ngos page
    And I select create ngo
    And I submit a new ngo
    Then I should see the new ngo in the list

  Scenario: Changing the name of a ngo
    And There is a ngo in the page
    When I submit a new ngo
    And I change the name of the ngo
    Then I should see the new name of ngo in the list

  @javascript
  Scenario: Removing a ngo
    When I submit a new ngo
    When I delete the ngo
    Then I should not see the ngo in the list

  Scenario: Reading a ngo
    When I submit a new ngo
    When I visit the ngos page
    And I select a ngo
    Then I should see the ngo details

  Scenario: Edit NGO as no admin
    And There is a ngo in the page
    And There are several ngos
    And I am an not admin user
    When I Try to edit the NGO
    Then I see an error message

Scenario: Creating NGO with a new user
  Given I am not signed in
  When I sign up
  And I visit the ngos page
  And I submit a new ngo
  Then I should see the new ngo in the list

Scenario: Logging in with other user
  Given I am not signed in
  When I sign up
  And I visit the ngos page
  And I submit a new ngo
  And I should see the new ngo in the list
  And I sign out
  And I fill the sign in form with other user
  And I visit the ngos page
  Then I should not see the NGO page

@javascript
Scenario: Transforming an admin into a worker
  Given There are several ngos
  Given I am the admin of the NGO
  Given There is another admin of the NGO
  When I visit the NGO
  And I transform an admin to worker
  Then I should not see the admin in the NGO

Scenario: Transforming a worker into an admin
  Given There are several ngos
  Given I am the admin of the NGO
  Given There is a worker in the NGO
  When I visit the NGO
  And I transform a worker to admin
  Then I should see the new ngo's admin
@javascript
Scenario: Deleting a user from the NGO
  Given There are several ngos
  Given I am the admin of the NGO
  Given There is another user in the NGO
  When I visit the NGO
  And I delete a user
  Then I should not see the user deleted on the page
@javascript
Scenario: Transforming the last admin into a worker
  Given There are several ngos
  Given I am the admin of the NGO
  Given There is a worker in the NGO
  When I visit the NGO
  And I try to make me worker to the last admin
  Then I still need to be admin of this NGO
@javascript
Scenario: Deleting the last admin from the NGO
  Given There are several ngos
  Given I am the admin of the NGO
  Given There is a worker in the NGO
  When I visit the NGO
  And I delete the last admin on the NGO
  Then I still need to be admin of this NGO

  Scenario: Checking NGO details as a worker
    Given There are several ngos
    And   I am a worker of the ngo
    When  I visit the ngo page
    Then  I should see the ngo details

  Scenario: Checking NGO projects as a worker
    Given There are several ngos
    And   I am a worker of the ngo
    When  I visit the ngo projects page
    Then  I should see the ngo projects

  Scenario: Checking NGO users as a worker
    Given There are several ngos
    And   I am a worker of the ngo
    When  I visit the ngo page
    Then  I should see the ngo users details

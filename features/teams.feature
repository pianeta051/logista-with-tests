Feature: Teams
I should be able to add teams to my projects

Background:
  Given I am a registered user
  And I sign in
  And I am a NGO admin
  And The NGO has a project
  And The project has some teams

  Scenario: Listing teams
    When  I visit the project page
    Then  I should see a list of teams

  Scenario: Creating a team
    When  I create a new team
    Then  I should see the team in the project details

  Scenario: Editing a team
    When  I change the country of a team
    Then  I should see the new country in the team details

  @javascript
  Scenario: Deleting a team
    When  I delete a team
    Then  I should not see the team in the project details

  Scenario: Add participant to team
    Given I have a participant with the same country
    When  I add the participant to the team
    Then  I should see the participant in the team details

  Scenario: Add leader to team
    Given I have a participant with the same country
    When  I add the participant to the team as a leader
    Then  I should see the participant in the team details as a leader

  Scenario: Creating a participant for the team
    When  I create a new participant for the team
    Then  I should see the participant in the team details

  @javascript
  Scenario:Deleting participant to a team
    Given The team has a participant
    When  I delete the participant from the team
    Then  I should not see the participant in the team

  Scenario: Making a participant team leader
    Given The team has a participant
    When  I make the participant leader
    Then  I should see the participant in the team details as a leader

  Scenario: Making a team leader regular participant
    Given The team has a leader
    When  I make the leader become regular participant
    Then  I should see the participant in the team details

# Testing with worker users
Scenario: Showing teams with a worker assosiated
  When I am a worker
  And I am assosiated to this project
  Then I should see the team in list

# Scenario: Showing teams with a worker not assosiated
#   When I am a worker
#   Then I should not see the team in the list

Scenario: Editing a team with a worker assosiated
  Given I am a worker
  And I am assosiated to this project
  When I change the country of a team
  Then  I should see the new country in the team details
@javascript
Scenario: Deleting a team with a worker assosiated
  Given I am a worker
  And I am assosiated to this project
  When I delete a team
  Then  I should not see the team in the project details

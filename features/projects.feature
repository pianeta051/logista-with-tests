Feature: Projects
I should be able to publish and see projects in the app

Background:
  Given I am a registered user
  And I sign in
  And There is a ngo in the page
  And I am an admin user
  And There are several projects

    Scenario: Listing projects
      When I visit the projects page
      Then I should see a list of projects

    Scenario: Creating a project
      And I create a new project
      Then I should see the new project in the list

    Scenario: Changing the name of a project
      When I change the name of the project
      Then I should see the new name in the list

    @javascript
    Scenario: Removing a project
      When I delete a project
      Then I should not see the project in the list

    Scenario: Reading a project
      Given The project has some teams
      When  I visit a project's page
      Then  I should see the project details
      And   I should see the project teams

    Scenario: Future projects
      Given There are some future projects
      When  I search for the future projects
      Then  I should only see the future projects

    Scenario: Past projects
      Given There are some past projects
      When  I search for the past projects
      Then  I should only see the past projects

    Scenario: Current projects
      Given There are some current projects
      When  I search for the current projects
      Then  I should only see the current projects

    Scenario: Search projects
      Given There are some projects to search
      When I search a project
      Then I should see the project in the list of search

    @javascript
    Scenario: Search projects by start date
      Given There are some projects to search
      When I search a project by start date
      Then I should see the project in the list of search

    @javascript
    Scenario: Search projects by final day
      Given There are some projects to search
      When I search a project by final day
      Then I should see the project in the list of search

    @javascript
    Scenario: Search projects using all parameters
      Given There are some projects to search
      When I search a project using all parameters
      Then I should see the project in the list of search

    @javascript
    Scenario: Search projects by project_city
      Given There are some projects to search
      When I search a project by city
      Then I should see the project in the list of search
# Testing Workers user
    Scenario: Accesing to project with workers user
      Given There is a NGO and a project assosiated to the NGO
      When I am a worker
      Then I could see the projects button on the NGO page

    Scenario: Adding a worker to a project
      Given The NGO has a worker
      When  I add a worker to the project
      Then  I should see the new worker in the project

    Scenario: Deleting a worker from a project
      Given The NGO has a worker
      And   The worker is associated with a project
      When  I delete the worker from the project
      Then  I should not see the worker in the project

    Scenario: Listing projects as a worker
      Given There is a worker
      And   I log out as an admin and log in as a worker
      When  I try to list all project from all ngos
      Then  I should see the list

    Scenario: Checking a project as a worker
      Given There is a worker
      And   I log out as an admin and log in as a worker
      And   The worker is associated with the project
      When  I visit the project page
      Then  I should be able to see the project details

    Scenario: Checking a project teams list as a worker
      Given There is a worker
      And   I log out as an admin and log in as a worker
      And   The worker is associated with the project
      And   The project has some teams
      When  I visit the project page
      Then  I should be able to see the project teams list

    Scenario: Searching projects as a worker
      Given There is a worker
      And   I log out as an admin and log in as a worker
      When  I search projects
      Then  I should see the search results

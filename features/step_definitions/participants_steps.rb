Given("There are several participants") do
  FactoryBot.create_list(:participant, 5)
end

Given("There is a participant in the page") do
  @participant = FactoryBot.create(:participant)
end

Given("I am admin of the NGO") do
  NgoUser.create(ngo_id:@ngo1.id, user_id: @user.id, admin:true)
end

Given("The participant is related to my NGO") do
  @project = FactoryBot.create(:project, ngo: @ngo1)
  @team = FactoryBot.create(:team, project: @project)
  @team.add_participant(@participant)
end

Given("I am an worker of the ngo") do
  @ngo1.add_worker(@user)
end

Given("There is a participant in a team of my ngo") do
  @project = FactoryBot.create(:project, ngo_id:@ngo1.id)
  @team = FactoryBot.create(:team, project_id:@project.id)
  @participant = FactoryBot.create(:participant)
  @team.add_participant(@participant)
end

Given ("There are several participant in a team of my ngo") do
  @project = FactoryBot.create(:project, ngo_id:@ngo1.id)
  @team = FactoryBot.create(:team, project_id:@project.id)
  @participants = FactoryBot.create_list(:participant, 5)
  @participants.each do |participant|
    @team.add_participant(participant)
  end
  # @project2 = FactoryBot.create(:project, ngo_id:@ngo2.id)
  # @team2 = FactoryBot.create(:team, project_id:@project2.id)
  # @participants_of_ngo2 = FactoryBot.create_list(:participant, 5)
  # @participants_of_ngo2.each do |participant|
  #   @team.add_participant(participant)
  # end

end

Given("There is a project in the ngo") do
  @project = FactoryBot.create(:project, ngo_id:@ngo1.id)
end

Given("There is a team in the project") do
@team = FactoryBot.create(:team, project_id:@project.id)
end

Given("The user is associated with a project") do
 @project.add_worker(@user.id)
end

When("I submit a new participant") do
  @participant = FactoryBot.build(:participant)
  visit ngo_new_participant_path(@ngo1)
  fill_in 'participant_name', with: @participant.name
  fill_in 'participant_nationality', with: @participant.nationality
  fill_in 'participant_residence', with: @participant.residence
  fill_in 'participant_country_code', with: @participant.country_code
  fill_in "participant_telephone", with: @participant.telephone
  fill_in "participant_email", with: @participant.email
  fill_in "participant_date_of_birth", with: @participant.date_of_birth
  select @participant.sex, from: "participant_sex"
  click_button 'Save'
end

When("I change the name of the participant") do
  visit edit_ngo_participant_path(@ngo1.id, @participant)
  fill_in "participant_name", with: "Karen Irina"
  click_button "Save"
  @participant.name = "Karen Irina"
end

When("I delete the participant") do
  visit ngo_path(@ngo1)
  click_on I18n.t('layouts.header.participants')
  page.find('li.list-group-item', text: @participant.name).click_on('Delete')
  page.driver.browser.switch_to.alert.accept
end

When("I select the participant") do
  page.find('li.list-group-item', text: @participant.name).click_on(@participant.name)
end

When("I visit the participants page") do
  visit ngo_path(@ngo1)
  click_on I18n.t('ngos.show.participants')
end

Then("I should see a list of participants of that ngo") do
  Participant.in_ngo(@ngo1.id).each do |participant|
    expect(page).to have_content(participant.name)
  end
end

Then("I should not see the participant in the list") do
  visit ngo_path(@ngo1)
  click_on I18n.t('layouts.header.participants')
  expect(page).to_not have_content(@participant.name)
end

Then("I should see the new participant in the list") do
  visit ngo_path(@ngo1)
  click_on I18n.t('layouts.header.participants')
  expect(page).to have_content(@participant.name)
end

Then("I should see the new participant in the project list") do
  visit project_path(@project.id)
  expect(page).to have_content(@participant.name)
end

Then("I should see the participant details") do
  expect(page).to have_content(@participant.name)
  expect(page).to have_content(@participant.nationality)
  expect(page).to have_content(@participant.residence)
  expect(page).to have_content(@participant.telephone)
  expect(page).to have_content(@participant.email)
  expect(page).to have_content(@participant.date_of_birth)
  expect(page).to have_content(@participant.sex)
end

When("I add a new participant to a team of the ngo") do
  @team.add_participant(@participant)
end

Then("I should not see the participant in the project list") do
  expect(page.find('div#team-participants')).not_to have_content(@participant.name)
end

Then("I should see the participant as a leader in the list") do
  expect(page.find('div#leaders')).to have_content(@participant.name)
end

When("I create a new participant for a team") do
  visit ngo_new_participant_path(@ngo1, team_id: @team.id)
  fill_in 'participant_name', with: @participant.name
  fill_in 'participant_nationality', with: @participant.nationality
  fill_in 'participant_residence', with: @participant.residence
  fill_in 'participant_country_code', with: @participant.country_code
  fill_in "participant_telephone", with: @participant.telephone
  fill_in "participant_email", with: @participant.email
  fill_in "participant_date_of_birth", with: @participant.date_of_birth
  select @participant.sex, from: "participant_sex"
  click_button 'Save'
end

When("I visit the participant details page") do
   visit ngo_participant_path(@ngo1.id, @participant)
end

When("I edit the participant details page") do
  visit edit_ngo_participant_path(@ngo1.id, @participant)
  fill_in "participant_name", with: "New Name"
  click_button "Save"
  @participant.name = "New Name"
end

Then("I should see the new participant details") do
  visit ngo_participant_path(@ngo1.id, @participant)
  expect(page).to have_content(@participant.name)
end

Then("I should see the participants associated to my ngo") do
  @participants.each do |participant|
    expect(page).to have_content(participant.name)
    # expect(page.find('.list-group-item', text: participant.name)).to have_content(I18n.t('teams.show.buttons.edit'))
    # expect(page.find('.list-group-item', text: participant.name)).to have_content(I18n.t('teams.show.buttons.delete'))
  end
end

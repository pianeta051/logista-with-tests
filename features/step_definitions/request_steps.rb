
When("I join to a NGO") do
  @ngo = Ngo.last
  click_link "Join"
  page.find(".list-group-item", text: @ngo.name).click_on "Join"
end

Then("I should see a Requested button") do
  visit join_ngos_path
  expect(page).to have_selector(:link_or_button, 'Requested')

end

Given ("There are requests for my NGO") do
  @user2 = FactoryBot.create(:user)
  @request = FactoryBot.create(:request, ngo_id: @ngo1.id, user_id:@user2.id)
end

Given("I am a user administrator of an NGO") do
  NgoUser.create(user_id: @user.id, ngo_id: @ngo1.id, admin: true)
end

When("I visit the request page") do
  visit requests_path
end

Then("I should see all my request for other users") do
  expect(page).to have_css('p', text: "NGO: "+@ngo1.name)
end

When("I accept a request") do
  page.find('.list-group-item', text:"NGO: "+@ngo1.name).click_on "Accept"
end

Then("I should see the new user of my NGO") do
  visit ngo_path(@ngo1.id)
  expect(page).to have_css('.list-group-item', text: @user2.name)
end

When("I reject a request") do
  page.find('.list-group-item', text: "NGO: "+@ngo1.name).click_on "Reject"
end

Then("I should not see the new user of my NGO") do
  visit ngo_path(@ngo1.id)
  expect(page).not_to have_css('.list-group-item', text: @user2.name)
end

Given("I am a NGO admin") do
  @ngo = FactoryBot.create(:ngo)
  @ngo.add_admin(@user)
end

Given("I have a participant with the same country") do
  @participant = FactoryBot.create(:participant, nationality: @team.country)
end

Given("The NGO has a project") do
  @project = FactoryBot.create(:project, ngo: @ngo)
end

Given("The project has some teams") do
  FactoryBot.create_list(:team, 3, project: @project)
  @team = @project.teams.first
end

Given("The team has a leader") do
  @participant = FactoryBot.create(:participant, nationality: @team.country)
  @team.add_participant(@participant, true)
end

Given("The team has a participant") do
  @participant = FactoryBot.create(:participant, nationality: @team.country)
  @team.add_participant(@participant)
end

When("I add the participant to the team") do
  visit team_path(@team)
  page.find('li.list-group-item', text: @participant.name).click_on('Add')
end

When("I add the participant to the team as a leader") do
  visit team_path(@team)
  page.find('li.list-group-item', text: @participant.name).check('leader')
  page.find('li.list-group-item', text: @participant.name).click_on('Add')
end

When("I change the country of a team") do
  @old_country = @team.country
  @new_country = "West Siberia"
  visit edit_team_path(@team)
  fill_in "team_country", with: @new_country
  click_on "Save"
end

When("I create a new participant for the team") do
  visit team_path(@team)
  click_on "New participant"
  @participant = FactoryBot.build(:participant)
  fill_in "participant_name", with: @participant.name
  fill_in "participant_country_code", with: @participant.country_code
  fill_in "participant_telephone", with: @participant.telephone
  fill_in "participant_email", with: @participant.email
  fill_in "participant_date_of_birth", with: @participant.date_of_birth
  select @participant.sex, from: "participant_sex"
  click_on "Save"
end

When("I create a new team") do
  @team = FactoryBot.build(:team)
  visit project_path(@project)
  click_on "New Team"
  fill_in "team_country", with: @team.country
  fill_in "team_budget", with: @team.budget
  click_on "Save"
end

When("I delete the participant from the team") do
  visit team_path(@team)
  page.find('div#non-leaders', text: @participant.name).click_on('Delete')
  page.driver.browser.switch_to.alert.accept
end

When("I delete a team") do
  visit team_path(@team)
  click_on 'Delete'
  page.driver.browser.switch_to.alert.accept
end

When("I make the leader become regular participant") do
  visit team_path(@team)
  page.find('.participant', text: @participant.name).click_on('Make regular')
end


When("I make the participant leader") do
  visit team_path(@team)
  page.find('.participant', text: @participant.name).click_on('Make leader')
end

When("I visit the project page") do
  visit project_path(@project)
end

Then("I should not see the participant in the team") do
  visit team_path(@team)
  expect(page.find('.card', text: 'List of participants')).not_to have_link(@participant.name)
end

Then("I should not see the team in the project details") do
  visit project_path(@project)
  expect(page).to_not have_content(@team.country)
end

Then("I should see a list of teams") do
  @project.teams.each do |team|
    expect(page).to have_content(team.country)
  end
end

Then("I should see the new country in the team details") do
  visit team_path(@team)
  expect(page).to have_content(@new_country)
  expect(page).not_to have_content(@old_country)
end

Then("I should see the team in the project details") do
  visit project_path(@project)
  expect(page).to have_content(@team.country)
end

Then("I should see the participant in the team details") do
  visit team_path(@team)
  expect(page.find('#non-leaders')).to have_text(@participant.name)
end

Then("I should see the participant in the team details as a leader") do
  visit team_path(@team)
  expect(page.find('#leaders')).to have_text(@participant.name)
end

When("I am assosiated to this project") do
  @project.add_worker(@user.id)
end

Then("I should see the team in list") do
  visit project_path(@project)
  expect(page).to have_css('.card', text:@team.country)
end


Then("I should not see the team in the list") do
  visit project_path(@project)
  expect(page).not_to have_css('.card', text:@team.country)
  expect(page).to have_css('.alert', text:'You are not authorized to see this page.')
end


When("I want to edit a team") do
  visit edit_team_path(@project)
end

Then("I should not be able to edit a team") do
  expect(page).not_to have_css('.btn', text:'Edit')
end

When("I want to delete a team") do
  visit team_path(@project)
end

Then("I should not be able to delete a team") do
  expect(page).not_to have_css('.btn', text:'Delete')
end

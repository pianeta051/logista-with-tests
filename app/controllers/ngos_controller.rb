class NgosController < ApplicationController

  before_action :set_ngo, except: [:my_ngos, :select, :new, :create]
  before_action :authenticate_user!
  before_action :is_admin, except: [:show, :my_ngos, :select, :new, :create, :projects, :join]
  before_action :set_breadcrumbs

  # GET /my_ngos
  def my_ngos
    @ngos = current_user.ngos
  end

  def select
    @ngos= Ngo.without_user(current_user.id)
  end
  # GET /ngos/1
  # GET /ngos/1.json
  def show
    @check= params[:check]
    @breadcrumbs << {text: @ngo.name, path: ngo_path(@ngo)}
  end

  # GET /ngos/new
  def new
    @breadcrumbs << {text: t(".breadcrumb_new"), path: new_ngo_path}
    @ngo = Ngo.new
  end

  # GET /ngos/1/edit
  def edit
    @breadcrumbs << {text: @ngo.name, path: ngo_path(@ngo)}
    @breadcrumbs << {text: t(".breadcrumb_edit"), path: edit_ngo_path(@ngo)}
  end

  # POST /ngos
  # POST /ngos.json
  def create
    @ngo = Ngo.new(ngo_params)
    if @ngo.save
      @ngo_user = NgoUser.create(user_id: current_user.id, ngo_id:@ngo.id, admin:true)
      if @ngo_user.save
        redirect_to @ngo, flash: {success: t(".success")}
      else
        redirect_to @ngo, flash: {warning: t(".warning")}
      end
    else
      render :new
    end
  end

  # PATCH/PUT /ngos/1
  # PATCH/PUT /ngos/1.json
  def update
    respond_to do |format|
      if @ngo.update(ngo_params)
        format.html { redirect_to @ngo, notice: t(".notice") }
        format.json { render :show, status: :ok, location: @ngo }
      else
        format.html { render :edit }
        format.json { render json: @ngo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ngos/1
  # DELETE /ngos/1.json
  def destroy
    @ngo.destroy
    respond_to do |format|
      format.html { redirect_to my_ngos_path, notice: t(".notice") }
      format.json { head :no_content }
    end
  end

  def join
    @breadcrumbs << {text: t(".breadcrumb_join"), path: join_ngo_path(@ngo)}
    if !(@ngo.users.pluck(:id).include?(current_user.id))
      @request = Request.create(user_id: current_user.id, ngo_id:@ngo.id)
      if @request.save
        flash.now[:notice] = t(".notice")
        @ngos= Ngo.without_user(current_user.id)
        render :select
      else
        redirect_to ngos_url, warning: t(".warning")
      end
    else
      flash.now[:notice] = t(".warning")
      @ngos= Ngo.without_user(current_user.id)
      render :select
    end

  end

  def make_admin
    if @ngo.is_admin?(User.find(params[:user]))
      redirect_to ngo_path(params[:ngo_id]), notice: "This User Already is admin."
    else
      if !@ngo.is_admin?(current_user)
        redirect_to root_path
      else
        NgoUser.find_by(user_id:params[:user], ngo_id:@ngo.id).update(admin: true)
        redirect_to ngo_path(params[:ngo_id]), notice: t(".notice")
      end
    end
  end

  def make_worker
    if @ngo.last_admin
      redirect_to ngo_path(@ngo.id), notice: t(".warning")
    else
      NgoUser.find_by(user_id:params[:user], ngo_id:@ngo.id).update(admin: false)
      redirect_to ngo_path(@ngo.id), notice: t(".notice")
    end
  end

  def destroy_association
    user = User.find(params[:user])
    if @ngo.last_admin && @ngo.is_admin?(user)
      message = t(".notice.delete")
    elsif @ngo.remove_user(params[:user])
      message = t(".notice.disassociate")
    else
      message = t(".notice.error")
    end
    redirect_to @ngo, notice: message
  end

  def projects
    @message_when_empty = t(".notice")
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ngo
      if params[:id]
        @ngo = Ngo.find(params[:id])
      else
        @ngo = Ngo.find(params[:ngo_id])
      end
    rescue ActiveRecord::RecordNotFound
      redirect_to root_path, notice: 'This ngo does not exist'
    end

    def is_admin
      unless @ngo.is_admin?(current_user)
        redirect_to root_path, notice: t(".is_admin.notice")
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ngo_params
      params.require(:ngo).permit(:name, :country, :image)
    end

    def ngo_user_params
      params.require(:ngouser).permit(:user_id, :ngo_id, :admin)
    end

    def set_breadcrumbs
      @breadcrumbs =
      [
        {text:  t(".set_breadcrumbs.index"), path: root_path },
        {text: "Ngos", path: my_ngos_path }
      ]
    end
end

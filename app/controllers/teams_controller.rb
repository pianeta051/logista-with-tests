class TeamsController < ApplicationController
  before_action :set_team, except: [:new, :create]
  before_action :set_project, only: [:new, :create, :edit, :update]
  before_action :set_breadcrumbs
  before_action :set_participant, only:[:add_participant, :remove_participant, :make_leader, :make_regular]
  before_action :authenticate_user!
  before_action :check_user_access, only: [:new, :edit, :show]

  # GET /teams/new
  def new
    @breadcrumbs << {text: t(".breadcrumb_new"), path: new_team_path}
    @team = Team.new
  end

  # GET /teams/1/edit
  def edit
    @breadcrumbs << {text: t(".breadcrumb_edit"), path: edit_team_path(@team)}
  end

  def show
  end

  # POST /teams
  def create
    @team = Team.new(team_params)
    @team.project_id = params[:project_id]
    if @team.save
      redirect_to project_path(@team.project), flash: {success: t(".success")}
    else
      render :new
    end
  end

  # PATCH/PUT /teams/1
  def update
    if @team.update(team_params)
      redirect_to @team, flash: {success: t(".success")}
    else
      render :edit
    end
  end

  # DELETE /teams/1
  def destroy
    @team.destroy
    flash[:success] = t(".success")
    redirect_to project_path(@team.project)
  end

  def add_participant
    leader = params[:leader]  ? true : false
    pt = ParticipantTeam.new(team_id: @team.id, participant_id: @participant.id, leader: leader)
    if pt.save
      redirect_to @team, notice: t(".success")
    else
      redirect_to @team, error: t(".error")
    end
  end

  def remove_participant
    ParticipantTeam.find_by(team_id: @team.id, participant_id: @participant.id).destroy
    redirect_to @team, flash: { success: t(".success") }
  end

  def make_leader
    @team.make_leader(@participant)
    redirect_to @team
  end

  def make_regular
    @team.make_regular(@participant)
    redirect_to @team
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team
      @team = Team.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      redirect_to root_path, flash: {error: t(".error")}
    end

    def set_project
      if(params[:project_id])
        @project = Project.find(params[:project_id])
      elsif @team
        @project = @team.project
      else
      redirect_to root_path, flash: {error: t(".error")}
      end
    end

    def set_participant
      @participant = Participant.find(params[:participant_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def team_params
      params.require(:team).permit(:country, :budget, :project_id)
    end

    def set_breadcrumbs
      project = @team ? @team.project : Project.find(params[:project_id])
      ngo = project ? project.ngo : Project.find(params[:ngo_id])
      @breadcrumbs =
      [
        {text: t(".set_breadcrumbs.index"), path: root_path },
        {text: ngo.name, path: ngo_path(ngo)},
        {text: t(".set_breadcrumbs.title"), path: ngo_projects_path(ngo) },
        {text: project.name, path: new_ngo_project_path(project)}
      ]
      if @team
        @breadcrumbs << { text: @team.country, path: team_path(@team) }
      end
    end

    def check_user_access
      project = @project || @team.project
      if !project.ngo.is_admin?(current_user) && !project.has_worker?(current_user.id)
        redirect_to root_path, flash: { error: t(".check_user_access.error") }
      end
    end
end

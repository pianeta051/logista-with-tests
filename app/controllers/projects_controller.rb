class ProjectsController < ApplicationController

  before_action :set_project, only: [:show, :edit, :update, :destroy, :add_user, :delete_user]
  before_action :set_ngo, except: [:index, :search]
  before_action :authenticate_user!, except: [:index, :show, :search]
  before_action :check_admin, only: [:new, :edit, :create, :update, :destroy, :add_user, :delete_user]
  before_action :set_breadcrumbs
  # GET /projects
  # GET /projects.json
  def index
    @message_when_empty =t(".empty_message")
    @active = params[:list] ? params[:list] : 'all'
    case @active
    when 'future'
      @projects = Project.future
    when 'past'
      @projects = Project.past
    when 'current'
      @projects = Project.current
    else
      @projects = Project.all
    end
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    @breadcrumbs << {text: @project.name}
  end

  # GET /projects/new
  def new
    @breadcrumbs << {text: t(".breadcrumb_new"), path: new_ngo_project_path(@ngo)}
    @project = Project.new
    @project.ngo = @ngo
  end

  # GET /projects/1/edit
  def edit
    @breadcrumbs << {text: @project.name, path: project_path(@project) }
    @breadcrumbs << {text: t(".breadcrumb_edit"), path: edit_project_path(@project.ngo) }
  end

  # POST /projects
  def create
    @project = Project.new(project_params)
    @project.city = @project.city.titleize
    @project.name = @project.name.titleize
    @project.ngo = @ngo
      if @project.save
        redirect_to project_path(@project), flash: {success: t(".success")}
      else
        render :new
      end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    if @project.update(project_params)
      redirect_to project_path(@project), success:t(".success")
    else
      render :edit
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, flash: {success: t(".success") }}
      format.json { head :no_content }
    end
  end

  def search
    @projects = Project.all
    @query = params[:query]
    @start_day = params[:start_day]
    @final_day = params[:final_day]
    @city = params[:city]
    @ngo_id = params[:ngo_id]

    unless @query.blank?
      @projects = Project.all.where("name like ?", "%"+@query+"%")
    end
    unless @start_day.blank?
      @projects = @projects.where("start_day >= ?", @start_day)
    end
    unless @final_day.blank?
      @projects = @projects.where("final_day <= ?", @final_day)
    end
    unless @city.blank?
      @projects = @projects.where(city: @city)
    end
    unless @ngo_id.blank?
      @projects = @projects.where(ngo_id: @ngo_id)
    end
  end

  def add_user
    if @project.has_worker?(params[:user])
      redirect_to project_path(@project), flash: {success:"User has already added"}
    else
      if @project.add_worker(params[:user])
       # if @project.add_worker(params[:user]) ...
       redirect_to project_path(@project), flash: {success:"Successfully added"}
     else
       redirect_to project_path(@project), flash: {error:"User was not added"}
     end
    end
  end

  def delete_user
    if @project.has_worker?(params[:user])
      ProjectUser.where(project_id: @project.id, user_id: params[:user]).destroy_all
      redirect_to project_path(@project), flash: {success:"Successfully destroyed"}
    else
      redirect_to project_path(@project), flash: {success:"User has already destroyed"}
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.

    def set_ngo
      @ngo= Ngo.where(id: params[:ngo_id]).first || @project.try(:ngo)
      unless @ngo
        redirect_to not_found_path
      end
    end

    def set_project
      @project = Project.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      redirect_to not_found_path
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      form_params = params.require(:project).permit(:category, :name, :city, :code, :country)
      if params[:project][:dates] && params[:project][:dates] != ""
        dates = params[:project][:dates].split(" ")
        form_params[:start_day] = dates[0].to_date
        form_params[:final_day] = dates[1].to_date
      else
        form_params[:start_day] = nil
        form_params[:final_day] = nil
      end
      form_params[:ngo_id] = @project.try(:ngo_id) or @ngo.id
      return form_params
    end

    def set_breadcrumbs
      @breadcrumbs = @ngo ? [
        {text: t(".set_breadcrumbs.index"), path: root_path },
        {text: @ngo.name, path: ngo_path(@ngo)},
        {text: t(".set_breadcrumbs.title"), path: ngo_projects_path(@ngo)}
        ] : [
        {text: t(".set_breadcrumbs.index"), path: root_path },
        {text: t(".set_breadcrumbs.title"), path: projects_path }
        ]
    end

    def check_admin
      unless @ngo.is_admin?(current_user)
        redirect_to projects_path, flash: { notice: t('.error')}
      end
    end
end

include Devise::Controllers::Helpers
class ApplicationController < ActionController::Base
  before_action :set_locale

  private

  def set_locale
    if cookies[:locale] && I18n.available_locales.include?(cookies[:locale].to_sym)
      l = cookies[:locale].to_sym
    else
      l = I18n.default_locale
      cookies.permanent[:locale] = l
    end
    I18n.locale = l
  end

  protected

  def authenticate_user!(opts = {})
    if user_signed_in?
      super
    else
      redirect_to sign_in_path
    end
  end
end

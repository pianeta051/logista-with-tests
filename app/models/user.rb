# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string
#

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :ngo_users
  has_many :ngos, through: :ngo_users
  has_many :project_users

  validates :name, presence: true

  scope :in_ngo, -> (user_id) { find(user_id)}

  # Returns a collection of NGOs not related to self
  def ngos_not_related
    Ngo.where.not(id: NgoUser.where(user_id: id).pluck(:ngo_id))
  end

  def can_edit?(participant)
    (self.ngos.to_a & participant.ngos.to_a).any?
  end

end

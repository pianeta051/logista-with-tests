# == Schema Information
#
# Table name: ngo_users
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  ngo_id     :integer
#  admin      :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class NgoUser < ApplicationRecord
  belongs_to :user
  belongs_to :ngo
end

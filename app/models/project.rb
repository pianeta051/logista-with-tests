# == Schema Information
#
# Table name: projects
#
#  id         :integer          not null, primary key
#  name       :string
#  start_day  :date
#  final_day  :date
#  city       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  category   :string
#  code       :string
#  country    :string
#  ngo_id     :integer
#

class Project < ApplicationRecord

  validates :category, presence: true, length: {maximum:100}
  validates :name, presence: true, length: {maximum: 200}
  validates :city, presence: true, length: {maximum: 100}
  validates :code, presence: true, length: {maximum: 100}
  validates :country, presence: true, length: {maximum: 100}
  validates :start_day, presence:true
  validates :final_day, presence:true

  validate :final_later_than_start
  has_many :teams, dependent: :destroy, inverse_of: :project
  has_many :participans
  belongs_to :ngo
  has_many :project_users
  # translates :category, :name, :city, :code, :country, :start_day, :final_day
  # attribute :category
  # attribute :name
  # attribute :city
  # attribute :code
  # attribute :country
  # attribute :start_day
  # attribute :final_day
  # globalize_accessors locales: [:en, :es], attributes: [:category, :name, :city, :code, :country, :start_day, :final_day]

  scope :current, -> {where('start_day < ?',  Time.now).where('final_day > ?', Time.now) }
  scope :future, -> {where('start_day > ?',  Time.now)}
  scope :past, -> {where('final_day < ?',  Time.now)}
  #scope :with_user, (user_id) -> {} # Lista de Project que están asociados al user (tanto worker como admin)
  #scope :with_worker, (user_id) -> {} # Lista de Project que tienen asociado ese worker

  def final_later_than_start
    return if [final_day.blank?, start_day.blank?].any?
    if final_day < start_day
      errors.add(:final_day, 'Debe ser superior a la fecha inicio')
    end
  end

  def has_worker?(user_id)
    ProjectUser.where(project_id: self.id, user_id: user_id).count > 0
  end

  def can_edit?(user)
    ngo.is_admin?(user) || self.has_worker?(user.id)
  end

  # add_worker
  # 1. Comprueba que el user que le pasan es worker de la ngo a la que pertenece este proyecto
  # 2. Si está ya asociado, no hace nada
  # 3. Si se cumple 1 y no est asociado, lo asocia
  def add_worker(user_id)
    user_id = user_id.to_i
    if !self.ngo.is_admin?(User.find(user_id)) && !self.has_worker?(user_id) && self.ngo.workers.pluck(:id).include?(user_id)
      ProjectUser.create(project_id: self.id, user_id: user_id)
      return true
    else
      return false
    end
  end
end

# == Schema Information
#
# Table name: requests
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  ngo_id     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Request < ApplicationRecord
  belongs_to :user
  belongs_to :ngo

  scope :to_user, ->(user_id) {where(ngo_id: Ngo.with_admin(user_id).pluck(:id))}
end

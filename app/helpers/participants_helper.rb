module ParticipantsHelper
  def sex_for_select(selected)
    if selected.nil?
      options_for_select(["Male", "Female", "Other"])
    else
      options_for_select(["Male", "Female", "Other"], selected)
    end
  end
end

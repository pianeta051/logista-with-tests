# == Schema Information
#
# Table name: ngos
#
#  id         :integer          not null, primary key
#  name       :string
#  country    :string
#  image      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Ngo, type: :model do
  it { is_expected.to have_db_column(:name).of_type(:string)}
  it { is_expected.to have_db_column(:country).of_type(:string)}
  it { is_expected.to have_db_column(:image).of_type(:string)}
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:country) }
  it { is_expected.to validate_length_of(:name).is_at_most(200) }
  it { is_expected.to validate_length_of(:country).is_at_most(200) }

  describe "Methods" do
    before(:all) do
      @ngo = FactoryBot.create(:ngo)
    end
    describe :admins do
      # When there is only one admin, the total admin count must be 1
      it 'should count admins' do
        @ngo.add_admin(FactoryBot.create(:user))
        expect(@ngo.admins.count).to eq(1)
      end

      # When there are no admins, it returns an empty relation
      it 'should retuns empty if there are no admins' do
        create_worker
        @ngo.add_worker(@worker)
        expect(@ngo.admins.count).to eq(0)
      end

      # When there are some admins and some workers, it returns only admins
      it 'it returns only admins' do
        FactoryBot.create_list(:ngo_user, 3, admin: true, ngo_id: @ngo.id)
        FactoryBot.create_list(:ngo_user, 5, admin: false, ngo_id: @ngo.id)
        # Checking that it returns a list of 3 admins
        expect(@ngo.admins.count).to eq(3)

      end

      it 'returns all the admins' do
        @ngo= FactoryBot.create(:ngo)
        admins = FactoryBot.create_list(:user, 3)
        admins.each do |admin|
          @ngo.add_admin(admin)
        end
        # Checking that every admin is included in the list
        admins.each do |admin|
          expect(@ngo.admins).to include(admin)
        end
      end
    end

    describe :is_admin? do
      # When I want to check an admin, it return true
      it 'returns true when passing an admin' do
        admin = FactoryBot.create(:user)
        @ngo.add_admin(admin)
        expect(@ngo.is_admin?(admin)).to eq(true)
      end

      it 'returns false when passing a worker' do
        create_worker
        @ngo.add_worker(@worker)
        expect(@ngo.is_admin?(@worker)).to eq(false)
      end

      it 'returns false when passing a user that is not related to the ngo' do
        create_user
        expect(@ngo.is_admin?(@user)).to eq(false)
      end
    end

    describe :remove_user do
      it 'decrements the number of users when passing a user that is related to the ngo' do
        FactoryBot.create_list(:ngo_user, 4, ngo_id: @ngo.id)
        create_worker
        @ngo.add_worker(@worker)
        expect{
          @ngo.remove_user(@worker)
        }.to change{@ngo.users.count}.by(-1)
      end

      it 'does not change the number of users when passing a user that is not related to the ngo' do
        FactoryBot.create_list(:ngo_user, 4, ngo_id: @ngo.id)
        create_user
        expect{
          @ngo.remove_user(@user)
        }.not_to change{@ngo.users.count}
      end

      it 'does not change the number of users when passing a user that doesn\'t exist' do
        FactoryBot.create_list(:ngo_user, 4, ngo_id: @ngo.id)
        expect{
          @ngo.remove_user(nil)
        }.not_to change{@ngo.users.count}
      end
    end

    describe :add_admin do
      before(:all) do
        FactoryBot.create_list(:ngo_user, 2, ngo_id: @ngo.id, admin:true)
        FactoryBot.create_list(:ngo_user, 2, ngo_id: @ngo.id, admin:false)
      end

      it 'increment the number of admin when passing a worker user' do
        create_user
        FactoryBot.create(:ngo_user, ngo_id:@ngo.id, user_id:@user.id, admin:false)
        expect{
          @ngo.add_admin(@user)
        }.to change{ @ngo.ngo_users.where(admin:true).count}.by(1)
      end

      it 'decrement the number of worker when passing a worker user' do
        create_user
        FactoryBot.create(:ngo_user, ngo_id: @ngo.id, user_id: @user.id, admin:false)
        expect{
          @ngo.add_admin(@user)
        }.to change{ @ngo.ngo_users.where(admin:false).count}.by(-1)
      end

      it 'does not change the number of users when passing a user that doesn\'t exist' do
        expect{
          @ngo.add_admin(nil)
        }.not_to change{@ngo.users.count}
      end

      it 'increments the number of admin when passing a user not related to the ngo' do
        create_user
        expect{
          @ngo.add_admin(@user)
        }.to change{@ngo.ngo_users.where(admin: true).count}.by(1)
      end

      it 'does not change the number of admins when passing an admin user' do
        create_user
        FactoryBot.create(:ngo_user, user_id:@user.id, ngo_id: @ngo.id, admin:true)
        expect{
          @ngo.add_admin(@user)
        }.not_to change{@ngo.ngo_users.where(admin: true).count}
      end
    end

    describe :add_worker do
      before(:all) do
        FactoryBot.create_list(:ngo_user, 2, ngo_id: @ngo.id, admin:true)
        FactoryBot.create_list(:ngo_user, 2, ngo_id: @ngo.id, admin:false)
      end
      it 'increment the number of worker when passing a admin user' do
        create_user
        FactoryBot.create(:ngo_user, ngo_id:@ngo.id, user_id:@user.id, admin:true)
        expect{
          @ngo.add_worker(@user)
        }.to change{ @ngo.ngo_users.where(admin:false).count}.by(1)
      end

      it 'decrement the number of admin when passing a admin user' do
        create_user
        FactoryBot.create(:ngo_user, ngo_id: @ngo.id, user_id: @user.id, admin:true)
        expect{
          @ngo.add_worker(@user)
        }.to change{ @ngo.ngo_users.where(admin:true).count}.by(-1)
      end

      it 'does not change the number of users when passing a user that doesn\'t exist' do
        expect{
          @ngo.add_worker(nil)
        }.not_to change{@ngo.users.count}
      end

      it 'increments the number of workers when passing an user not related to the ngo' do
        create_user
        expect{
          @ngo.add_worker(@user)
        }.to change{ @ngo.ngo_users.where(admin:false).count}.by(1)
      end

      it 'does not change the number of workers when passing a worker' do
        create_user
        FactoryBot.create(:ngo_user, ngo_id: @ngo.id, user_id: @user.id, admin:false)
        expect{
          @ngo.add_worker(@user)
        }.not_to change{ @ngo.ngo_users.where(admin:false).count}
      end
    end

    describe :last_admin do

      it 'return false when there are more than one admin' do
        FactoryBot.create_list(:ngo_user, 2, ngo_id: @ngo.id, admin:true)
        expect(
          @ngo.last_admin
        ).to eq(false)
      end

      it 'return true when it is the last admin' do
        NgoUser.delete_all
        FactoryBot.create_list(:ngo_user, 1, ngo_id: @ngo.id, admin:true)
        FactoryBot.create_list(:ngo_user, 2, ngo_id: @ngo.id, admin:false)
        expect(
          @ngo.last_admin
        ).to eq(true)
      end
    end
  end

  describe "Scopes" do
    describe :without_user do
      before :each do
        Ngo.delete_all
        @ngos=FactoryBot.create_list(:ngo, 10)
      end

      it 'returns all ngos when the user is not in any' do
        create_user
        expect(Ngo.without_user(@user.id).count).to eq(Ngo.all.count)
      end

      it 'returns an empty list of ngos when the user is in all of them' do
        create_user
        @ngos.each do |ngo|
          FactoryBot.create(:ngo_user, ngo_id: ngo.id, user_id:@user.id)
        end
        expect(Ngo.without_user(@user.id).empty?)
      end

      it 'returns the ngos in which the user is not in when it is in some of them' do
        create_user
        user2 = FactoryBot.create(:user)
        ngos_with_user2 = FactoryBot.create_list(:ngo, 5)
        ngos_with_user2.each do |ngo|
          ngo.add_admin(user2)
        end
        expect(Ngo.without_user(user2.id).count).to eq(10)
      end
    end

    describe :with_admin do
      it 'return all ngos where the user is admin' do
        create_user
        ngos_not_in = FactoryBot.create_list(:ngo,5)
        ngos_in = FactoryBot.create_list(:ngo, 2)
        ngos_in.each do |ngo|
          ngo.add_admin(@user)
        end
        expect(Ngo.with_admin(@user.id).count).to eq(ngos_in.count)
      end

      it 'does not return any ngo where the user is not admin' do
        create_user
        ngos = FactoryBot.create_list(:ngo,5)
        ngos.each do |ngo|
          ngo.add_worker(@user)
        end
        expect(Ngo.with_admin(@user.id).empty?)
      end
    end
  end

  def create_user
    @user = FactoryBot.create(:user)
  end

  def create_worker
    @worker = FactoryBot.create(:user)
  end
end

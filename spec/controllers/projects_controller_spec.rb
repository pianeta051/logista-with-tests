require 'rails_helper'

RSpec.describe ProjectsController, type: :controller do

  invalid_ids

  let (:project_lists) do
    ['all', 'future', 'past']
  end

  before(:all) do
    @ngo = FactoryBot.create(:ngo)
    @project = FactoryBot.create(:project, ngo: @ngo)
  end

  # Index renders a list of project, without any NGO filter.

  describe "GET #index" do
    context 'without params' do
      it "returns a success response without params" do
        FactoryBot.create_list(:project, 3)
        get :index
        expect(response).to be_successful
      end
    end

    context 'with valid params' do
      it "returns a success response with valid params" do
        project_lists.each do |list_name|
          get :index, params: { list: list_name }
          expect(response).to be_successful
        end
      end
    end
  end

  # Show renders a single project details. NGO id can be passed as a parameter
  # to display it in the page breadcrumbs.

  describe "GET #show" do
    context 'with valid project id' do
      context 'and without params' do
        it "returns a success response without params" do
          get :show, params: { id: @project.id }
          expect(response).to be_successful
        end
      end

      context 'and with ngo param' do
        it "returns a success response with a ngo param" do
          get :show, params: { id: @project.id, ngo: true }
          expect(response).to be_successful
        end
      end
    end

    context 'with invalid project id' do
      it "redirects to 404 the id is not valid" do
        get :show, params: { id: invalid_project }
        expect(response).to redirect_to not_found_path
      end
    end
  end

  # New renders the new project form template.
  # Only NGO admins can create new projects.

  describe 'GET #new' do
    context 'when not signed in' do
      it 'redirects to sign in page' do
        get :new, params: { ngo_id: @ngo.id }
        expect(response).to redirect_to sign_in_path
      end
    end

    context 'when signed in as a non-admin' do
      before(:each) do
        sign_in FactoryBot.create(:user)
      end

      it 'redirects to projects page' do
        get :new, params: { ngo_id: @ngo.id }
        expect(response).to redirect_to projects_path
      end
    end

    context 'when signed in as an admin' do
      before(:each) do
        admin = FactoryBot.create(:user)
        @ngo.add_admin(admin)
        sign_in admin
      end

      context 'with a valid ngo id' do
        it 'returns a success response' do
          get :new, params: { ngo_id: @ngo.id }
          expect(response).to be_successful
        end
      end

      context 'with an invalid ngo id' do
        it 'redirects to 404 page' do
          get :new, params: { ngo_id: invalid_ngo }
          expect(response).to redirect_to not_found_path
        end
      end
    end
  end

  # Edit renders the project edition form template.
  # Only NGO admins can edit its projects.

  describe 'GET #edit' do
    context 'when not signed in' do
      it 'redirects to sign in page' do
        get :edit, params: { id: @project.id }
        expect(response).to redirect_to sign_in_path
      end
    end

    context 'when signed in as non-admin' do
      before(:each) do
        sign_in FactoryBot.create(:user)
      end

      it 'redirects to projects page' do
        get :edit, params: { id: @project.id }
        expect(response).to redirect_to projects_path
      end
    end

    context 'when signed in as admin' do
      before(:each) do
        admin = FactoryBot.create(:user)
        @ngo.add_admin(admin)
        sign_in admin
      end

      context 'with a valid project id' do
        it 'returns a success response' do
          get :edit, params: { id: @project.id }
          expect(response).to be_successful
        end
      end

      context 'with an invalid project id' do
        it 'redirects to 404 page' do
          get :edit, params: { id: invalid_project }
          expect(response).to redirect_to not_found_path
        end
      end
    end
  end

  # Create receives the form data from new, and creates a new project.
  # Only NGO admins can create new project.
  # Create checks all the model validations before inserting anything in the DB.

  describe 'POST #create' do
    context 'when not signed in' do
      it 'redirects to sign in page' do
        post :create, params: {ngo_id: @ngo.id, project: FactoryBot.attributes_for(:project_from_form)}
        expect(response).to redirect_to sign_in_path
      end

      it 'does not create a new project' do
        expect {
          post :create, params: {ngo_id: @ngo.id, project: FactoryBot.attributes_for(:project_from_form)}
        }.not_to change(Project, :count)
      end
    end

    context 'when signed in as non-admin' do
      before(:each) do
        sign_in FactoryBot.create(:user)
      end

      it 'redirects to projects page' do
        post :create, params: {ngo_id: @ngo.id, project: FactoryBot.attributes_for(:project_from_form)}
        expect(response).to redirect_to projects_path
      end

      it 'does not create a new project' do
        expect {
          post :create, params: {ngo_id: @ngo.id, project: FactoryBot.attributes_for(:project_from_form)}
        }.not_to change(Project, :count)
      end
    end

    context 'when signed in as admin' do
      before(:each) do
        admin = FactoryBot.create(:user)
        @ngo.add_admin(admin)
        sign_in admin
      end

      context 'with valid params' do
        it 'redirects to project page' do
          post :create, params: {ngo_id: @ngo.id, project: FactoryBot.attributes_for(:project_from_form)}
          expect(response).to redirect_to project_path(Project.last)
        end

        it 'creates a new project' do
          expect {
            post :create, params: {ngo_id: @ngo.id, project: FactoryBot.attributes_for(:project_from_form)}
          }.to change(Project, :count).by(1)
        end
      end

      context 'with invalid params' do
        it 'renders the new project form' do
          post :create, params: {ngo_id: @ngo.id, project: FactoryBot.attributes_for(:invalid_project_from_form)}
          expect(response).to be_successful
        end

        it 'does not create a new project' do
          expect {
            post :create, params: {ngo_id: @ngo.id, project: FactoryBot.attributes_for(:invalid_project_from_form)}
          }.not_to change(Project, :count)
        end
      end

      context 'with an invalid ngo id' do
        it 'redirects to 404 page' do
        end

        it 'does not create a new project' do
        end
      end
    end
  end

  # Update receives the new project attributes from the form in the Edit view
  # and updates the project record in the database.
  # It performs and validation check first, and only updates if all model
  # constraints are met.
  # Only NGO admins can update their projects.

  describe 'PUT #update' do
    context 'when not signed in' do
      before(:each) do
        put :update, params: { id: @project.id, project: FactoryBot.attributes_for(:project_from_form)}
      end

      it 'redirects to sign in page' do
        expect(response).to redirect_to sign_in_path
      end

      it 'does not update the project' do
        expect {
          @project.reload
        }.not_to change{@project.attributes.except("created_at", "updated_at")}
      end
    end

    context 'when signed in as non-admin' do
      before(:each) do
        sign_in FactoryBot.create(:user)
        put :update, params: { id: @project.id, project: FactoryBot.attributes_for(:project_from_form)}
      end

      it 'redirects to the projects page' do
        expect(response).to redirect_to projects_path
      end

      it 'does not update the project' do
        expect {
          @project.reload
        }.not_to change{@project.attributes.except("created_at", "updated_at")}
      end
    end

    context 'when signed in as admin' do
      before(:each) do
        admin = FactoryBot.create(:user)
        @ngo.add_admin(admin)
        sign_in admin
      end

      context 'with valid params' do
        before(:each) do
          @new_attributes = FactoryBot.attributes_for(:project_from_form)
          put :update, params: { id: @project.id, project: @new_attributes }
        end

        it 'redirects to the project page' do
          expect(response).to redirect_to project_path(@project)
        end

        it 'updates the project' do
          @project.reload
          expect(@project.name).to eq(@new_attributes[:name])
        end
      end

      context 'with invalid params' do
        before(:each) do
          @project.reload
          @new_attributes = FactoryBot.attributes_for(:invalid_project_from_form)
          put :update, params: { id: @project.id, project: @new_attributes }
        end

        it 'renders the project edit page' do
          expect(response).to be_successful
        end

        it 'does not update the project' do
          expect {
            @project.reload
          }.not_to change{@project.attributes.except("id", "created_at", "updated_at")}
        end
      end

      context 'with an invalid project id' do
        before(:each) do
          @new_attributes = FactoryBot.attributes_for(:invalid_project_from_form)
          put :update, params: { id: invalid_project, project: @new_attributes }
        end

        it 'redirects to the 404 page' do
          expect(response).to redirect_to not_found_path
        end

        it 'does not update the project' do
          expect {
            @project.reload
          }.not_to change{@project.attributes.except("created_at", "updated_at")}
        end
      end
    end
  end

  # Destroy deletes a project database record and all its dependencies.
  # It leaves the database in a consistent state.
  # Only NGO admins can delete their projects.

  describe 'DELETE #destroy' do
    context 'when not signed in' do
      it 'redirects to sign in page' do
        delete :destroy, params: { id: @project.id }
        expect(response).to redirect_to sign_in_path
      end

      it 'does not delete the project' do
        expect {
          delete :destroy, params: { id: @project.id }
        }.not_to change(Project, :count)
      end
    end

    context 'when signed in as non-admin' do
      before(:each) do
        sign_in FactoryBot.create(:user)
      end

      it 'redirects to projects page' do
        delete :destroy, params: { id: @project.id }
        expect(response).to redirect_to projects_path
      end

      it 'does not delete the project' do
        expect {
          delete :destroy, params: { id: @project.id }
        }.not_to change(Project, :count)
      end
    end

    context 'when signed in as admin' do
      before(:each) do
        admin = FactoryBot.create(:user)
        @ngo.add_admin(admin)
        sign_in admin
      end

      context 'with valid project id' do
        it 'redirects to projects page' do
          delete :destroy, params: { id: @project.id }
          expect(response).to redirect_to projects_path
        end

        it 'deletes the project' do
          expect {
            delete :destroy, params: { id: @project.id }
          }.to change(Project, :count).by(-1)
        end

        it 'deletes its teams' do
          FactoryBot.create_list(:team, 5, project: @project)
          teams_number = @project.teams.count
          expect {
            delete :destroy, params: { id: @project.id }
          }.to change(Team, :count).by((-1)*teams_number)
        end
      end

      context 'with invalid project id' do
        it 'redirects to 404 page' do
          delete :destroy, params: { id: invalid_project }
          expect(response).to redirect_to not_found_path
        end

        it 'does not delete the project' do
          expect {
            delete :destroy, params: { id: invalid_project }
          }.not_to change(Project, :count)
        end
      end
    end
  end

  # Search renders a list of search results.
  # It has some optional search parameters: query, start_day, final_day and city
  # All users (even not signed in) can search.

  describe 'GET #search' do
    context 'when only one parameter is sent' do
      it 'returns a success response' do
        get :search, params: { query: 'science' }
        expect(response).to be_successful
      end
    end

    context 'when all the parameters are sent' do
      it 'return a success response' do
        get :search, params: {
          query: 'science',
          start_day: '01-01-2001',
          final_day: '01-02-2001',
          city: 'London',
          ngo_id: 1
        }
        expect(response).to be_successful
      end
    end

    context 'when no parameters are sent' do
      it 'returns a success response' do
        get :search
        expect(response).to be_successful
      end
    end
  end

  describe "PUT #add_user" do

    context 'when signed in as non-admin' do
      before(:each) do
        me = FactoryBot.create(:user)
        @ngo.add_worker(me)
        sign_in me
        @user = FactoryBot.create(:user)
        @ngo.add_worker(@user)
      end

      it 'redirects to projects page' do
        put :add_user, params: { id: @project.id, user: @user.id }
        expect(response).to redirect_to projects_path
      end

      it 'does not add the worker' do
        expect {
          put :add_user, params: { id: @project.id, user: @user.id }
        }.not_to change(ProjectUser, :count)
      end
    end

    context 'when signed in as an admin' do
      before(:each) do
        me = FactoryBot.create(:user)
        @ngo.add_admin(me)
        sign_in me
        @user = FactoryBot.create(:user)
        @ngo.add_worker(@user)
      end

      it "redirect to project page" do
        put :add_user, params: { id: @project.id, user: @user.id }
        expect(response).to redirect_to project_path(@project.id)
      end

      it "should add the worker" do
        expect {
          put :add_user, params: { id: @project.id, user: @user.id }
        }.to change(ProjectUser, :count).by(1)
      end
    end
  end

  describe "PUT #delete_user" do
    context 'when signed in as non-admin' do
      before(:each) do
        me = FactoryBot.create(:user)
        @ngo.add_worker(me)
        sign_in me
        @user = FactoryBot.create(:user)
        @ngo.add_worker(@user)
      end

      it 'redirects to projects page' do
        put :delete_user, params: { id: @project.id, user: @user.id }
        expect(response).to redirect_to projects_path
      end

      it 'does not delete the worker' do
        expect {
          put :delete_user, params: { id: @project.id, user: @user.id }
        }.not_to change(ProjectUser, :count)
      end
    end

    context 'when signed in as an admin' do
      before(:each) do
        me = FactoryBot.create(:user)
        @ngo.add_admin(me)
        sign_in me
        @user = FactoryBot.create(:user)
        @ngo.add_worker(@user)
      end

      it "redirect to project page" do
        put :delete_user, params: { id: @project.id, user: @user.id }
        expect(response).to redirect_to project_path(@project.id)
      end

      it "should delete the worker" do
        expect {
          put :delete_user, params: { id: @project.id, user: @user.id }
        }.to change(ProjectUser, :count).by(0)
      end
    end
  end
end

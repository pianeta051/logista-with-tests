require 'rails_helper'

RSpec.describe ProjectsHelper, type: :helper do
  describe 'categories_for_select' do
    it 'returns the list of categories' do
      expect(helper.categories_for_select).to contain_exactly('KA1', 'KA2', 'KA3')
    end
  end

  describe 'project_lists' do
    it 'returns the list filters for projects' do
      expect(helper.project_lists).to contain_exactly('all', 'future', 'current', 'past')
    end
  end
end
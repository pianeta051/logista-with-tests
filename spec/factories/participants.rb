# == Schema Information
#
# Table name: participants
#
#  id            :integer          not null, primary key
#  name          :string
#  nationality   :string
#  residence     :string
#  telephone     :integer
#  email         :string
#  date_of_birth :date
#  project_id    :integer
#  team_id       :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  sex           :string
#  country_code  :string
#

FactoryBot.define do
  factory :participant do
    name         { Faker::Name.name.titleize }
    nationality  { Faker::Address.country.titleize  }
    residence    { Faker::Address.country.titleize  }
    country_code { Faker::Number.decimal_part(digits: 3)}
    telephone    { Faker::Number.leading_zero_number(digits: 9)}
    email        { Faker::Internet.email }
    date_of_birth{ Faker::Date.birthday(min_age: 13, max_age: 65) }
    sex          { Faker::Gender.binary_type }
  end
end

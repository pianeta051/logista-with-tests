# == Schema Information
#
# Table name: projects
#
#  id         :integer          not null, primary key
#  name       :string
#  start_day  :date
#  final_day  :date
#  city       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  category   :string
#  code       :string
#  country    :string
#  ngo_id     :integer
#

#

FactoryBot.define do
  factory :project do
    category  {'KA1'}
    name      {Faker::Educator.unique.subject}
    start_day {Faker::Date.backward(days: 23)}
    country   {Faker::Address.country}
    code      {Faker::Code.nric(min_age: 2, max_age: 4)}
    final_day {Faker::Date.forward(days: 14)}
    city      {Faker::Nation.capital_city }
    ngo
    factory :project_with_teams do
      transient do
        teams_count {3}
      end
      after(:create) do |project, evaluator|
        create_list(:team, evaluator.teams_count, project:project)
      end
    end
  end

  factory :invalid_project, class: 'Project' do
    name           {""}
    code           {""}
    country        {""}
    start_day      {""}
    final_day      {""}
    city           {""}
  end

  # It sends the date range in the same way the form would
  factory :project_from_form, class: 'Project' do
    category  {'KA1'}
    name      {Faker::Educator.subject}
    country   {Faker::Address.country}
    code      {Faker::Code.nric(min_age: 2, max_age: 4)}
    city      {Faker::Nation.capital_city }
    dates     {'01-01-2001 01-02-2001'}
    ngo
  end

  factory :invalid_project_from_form, class: 'Project' do
    name           {""}
    code           {""}
    country        {""}
    start_day      {""}
    final_day      {""}
    city           {""}
    dates          {'01-02-2001 01-01-2001'}
  end
end

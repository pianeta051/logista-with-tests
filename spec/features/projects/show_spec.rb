require 'rails_helper'

RSpec.feature "Project show", :type => :feature do
  before(:each) do
    @admin = FactoryBot.create(:user)
    ngo = FactoryBot.create(:ngo)
    ngo.add_admin(@admin)
    @project = FactoryBot.create(:project, ngo: ngo)
    FactoryBot.create_list(:team, 5, project: @project)
  end

  scenario 'when the user is not signed in' do
    visit project_path(@project)

    # Regular users can see project details
    expect(page).to have_content(@project.name)
    expect(page).to have_content(@project.code)
    expect(page).to have_content(@project.country)
    expect(page).to have_content(@project.start_day)
    expect(page).to have_content(@project.final_day)
    expect(page).to have_content(@project.city)

    # But they can't see the project teams
    @project.teams.each do |team|
      expect(page).not_to have_content(team.country)
    end
  end

  scenario 'when the project has no teams' do
    sign_in @admin
    Team.where(id:@project.teams.ids).delete_all
    visit project_path(@project)
    expect(page).to have_content(I18n.t('projects.show.no_teams'))
  end
end

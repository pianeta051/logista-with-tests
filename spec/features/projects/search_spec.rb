require 'rails_helper'

RSpec.feature "Project search", :type => :feature do
  before(:each) do
    Faker::UniqueGenerator.clear
    @non_in_search_projects = FactoryBot.create_list(:project,9, name: 'a')
    @project_in_search  = FactoryBot.create(:project, name: 'b')
    @project = Project.first
    @ngo = FactoryBot.create(:ngo)
  end

  scenario 'when the user is not signed in' do
    visit projects_path
    fill_search_form
  end

  scenario 'when the user is log in ' do
    create_user
    visit projects_path
    fill_search_form
  end

  scenario 'when the project have an ngo' do
    create_user
    @project_in_search.update(ngo_id:@ngo.id)
    visit projects_ngo_path(@ngo.id)
    fill_search_form
  end

  scenario 'search without results' do
    create_user
    visit projects_path
    fill_in 'query', with: @project_in_search.name + "xxxx444"
    click_on I18n.t('projects.project_search_form.button_search')
    expect(page).to have_content(I18n.t('projects.search.alert'))
  end

  scenario 'when the search is in blanck' do
    create_user
    visit projects_path
    click_on I18n.t('projects.project_search_form.button_search')
    expect(page).to have_content(I18n.t('projects.search.no_field'))
  end
end

def create_user
  sign_in FactoryBot.create(:user)
end

def fill_search_form
  fill_in 'query', with: @project_in_search.name
  click_on I18n.t('projects.project_search_form.button_search')
  expect(page.find('#search-results')).to have_content(@project_in_search.name)
  @non_in_search_projects.each do |project|
    expect(page.find('#search-results')).not_to have_content(project.name)
  end
end

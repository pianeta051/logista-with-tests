require 'rails_helper'

RSpec.feature "Editing and Deleting teams", :type => :feature do
  before (:each) do
    @worker=FactoryBot.create(:user)
    @ngo = FactoryBot.create(:ngo)
    sign_in @worker
    @project = FactoryBot.create(:project, ngo_id:@ngo.id)
    @team = FactoryBot.create(:team, project_id:@project.id)
  end
  scenario "Trying to edit and delete team I'm not associated to" do
    visit project_path(@project)
    expect(page).to_not have_content(@team.country)
  end
end
